<?php 
 
namespace App\Http\Controllers;

use Response;
use Session;
use App\Shops;
use App\Deletedshops;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;
use App\Helpers\AppInitializationHelper;
use App\Helpers\ProductDataHelper;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class ShopController extends Controller
{
	public function getShopListings()
    {
		$shops = Shops::all();
        
        $view = View::make('admin.shops.index', ['shops' => $shops]);
        $contents = $view->render();
        
		$data['shops']  		    = 	$contents;
		
		echo json_encode($data);
	}
    
    public function getDeletedShopListings()
    {
		$shops = Deletedshops::all();
        
        $view = View::make('admin.deletedshops.index', ['deletedshops' => $shops]);
        $contents = $view->render();
        
		$data['deletedshops']  		    = 	$contents;
		
		echo json_encode($data);
	}
    
    public function shopCSV(){
        
            $shopsArray = DB::table('shops')
                    ->select('email')
					->get();
            
            $shopsArray =  json_decode(json_encode($shopsArray), true);
        
            $shops = array();
            foreach($shopsArray as $array){
                if(!empty($array['email'])){
               $shops[] = array('Emails' => $array['email'], 'status' => 'installed');
                }
                
            }
           
            $deletedShopsArray = DB::table('deletedshops')
                    ->select('email')
					->get();
            
            $deletedShopsArray =  json_decode(json_encode($deletedShopsArray), true);
        
            $deletedshops = array();
            foreach($deletedShopsArray as $array){
                if(!empty($array['email'])){
               $deletedshops[] = array('Emails' => $array['email'], 'status' => 'uninstalled');
                }             
            }
           
            $Emails = array_merge($shops,$deletedshops);
        
          
            $filename = "shops_".rand().".csv";
            $handle = fopen($filename, 'w+');
            fputcsv($handle, array('Email', 'Status'));
          
             foreach($Emails as $Data) {
                 if(!empty($Data['Emails'])){
                      fputcsv($handle, array($Data['Emails'], $Data['status']));
                 }    
                }

            fclose($handle);

            $headers = array(
                'Content-Type' => 'text/csv',
            );

            return Response::download($filename, $filename, $headers);
            exit;
      
    }
    
    
    public function saveCustomerReview(){
        
        $customer_review = Input::get('customer_review');
        $shopName = Input::get('shop_name');

        DB::table('shops')
            ->where('shop_name', $shopName)
            ->update([
                        'customer_review' => $customer_review,
                     ]);
    }	
    
    public function saveCustomerText(){
        
        $customer_text = Input::get('customer_text');
        $shopName = Input::get('shop_name');

        DB::table('shops')
            ->where('shop_name', $shopName)
            ->update([
                        'customer_text' => $customer_text,
                     ]);
    }	

}