<?php 
 
namespace App\Http\Controllers;
use App\Posts;
use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class PostController extends Controller
{
	public function getBlogIndex(){
		
		//Fetch Posts
		
		$posts = Posts::all();
		
		return view('frontend.blog.index', ['posts' => $posts]);
	}
	
	public function getSinglePost($post_id, $end ="frontend"){
		
		$post = Posts::find($post_id);
		
		//Fetch the post
		return view($end . '.blog.single', ['post' => $post]);
	}
	
	
	public function getCreatePost(){
		return view('admin.blog.create_post');
	}
	
	public function postCreatePost(Request $request){
		
		$this->validate($request, [
			'title' => 'required|unique:posts',
			'description' => 'required' 
		]);
		
		$post = new Posts();
		$post->title = $request['title'];
		$post->description = $request['description'];
		$post->save();
		
		$posts = Posts::all();
		
		$success = "Post successfully created";
		return view('admin.index', ['success' => $success, 'posts' => $posts]);

		//return redirect('admin/')->with(['success' => $success, 'posts' => $posts]);
		
		
	}
	
	public function getViewPost($id){
		
		$post = Posts::find($id);
		
		return view('admin.blog.single_post', ['post' => $post]);
		
	}
	
	public function getEditPost($id){
		
		$post = Posts::find($id);
		
		return view('admin.blog.edit_post', ['post' => $post]);
		
	}

	public function deletePost($post_id){
		
		$post = Posts::find($post_id);
		
		$post->delete();
		
		$posts = Posts::all();
		
		$success = "Post successfully Deleted";
		
		return view('admin.index', ['success' => $success, 'posts' => $posts]);
	}
	
	public function getUpdatePost(Request $request){
		
		$this->validate($request, [
			'title' => 'required|unique:posts',
			'description' => 'required' 
		]);
		
		$post = Posts::find($request['id']);
		$post->title = $request['title'];
		$post->description = $request['description'];
		$post->update();
		
		$posts = Posts::all();
		
		$success = "Post successfully updated";
		return view('admin.index', ['success' => $success, 'posts' => $posts]);
	}
	
	
	
}


?>