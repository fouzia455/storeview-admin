<?php 
 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Session;

class AdminController extends Controller
{
	public function getIndex(){
		
		$admin = Auth::user();
        
		$admin_id = Session::get('admin_id');
		
		if($admin_id !=='' && $admin_id !== null){
			
       		return view('admin.index');
		}
		
		else {
			
			return view('admin.login');
		}
		
	  
	}
	
	public function getLogin(){
		
		return view('admin.login');
	}
	
	public function postLogin(Request $request){
		
		$this->validate($request, [
			'email' => 'required',
			'password' => 'required' 
		]);
		
		if(!Auth::attempt(['email' => $request['email'], 'password' => $request['password']])){
			return redirect()->back()->with(['fail', 'Could not Log you in']);
		}
		
		$admin = Auth::user();
		
		Session::put('admin_id', $admin->id);
		
		return view('admin.index');	
	}
	
	public function getLogout(){
		
		Auth::logout();
		
		session()->forget('admin_id');
		session()->flush();
		
		return view('admin.login');
	}
	
}


?>