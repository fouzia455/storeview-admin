<!-- page specific plugin scripts -->
<script src="{!! URL::to('assets/js/jquery.dataTables.min.js') !!}"></script>
<script src="{!! URL::to('assets/js/jquery.dataTables.bootstrap.min.js') !!}"></script>

<script type="application/javascript">

jQuery(function($) {
				//initiate dataTables plugin
				var myTable = 
				$('#dynamic-table')
				//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
				.DataTable();
                } );
    
    $('#select_type').on('change', function (e) {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;

        if(valueSelected == 'shops'){
            $("#LoadingImage").show();
          $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
            method: 'GET',
            url: '/admin/public/shops',

            success: function(response) {
                $("#LoadingImage").hide();
                var response 		= JSON.parse(response);

                $(".listings").html('');

                $('.listings').append(response.shops);

            },
            error: function(jqXHR, textStatus, errorThrown) {
                $("#LoadingImage").hide();
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
        }
        else if(valueSelected == 'deleted_shops'){
            $("#LoadingImage").show();
          $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
            method: 'GET',
            url: '/admin/public/deletedshops',

            success: function(response) {
                $("#LoadingImage").hide();
                var response 		= JSON.parse(response);

                $(".listings").html('');

                $('.listings').append(response.deletedshops);

            },
            error: function(jqXHR, textStatus, errorThrown) {
                $("#LoadingImage").hide();
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
        }

    });
$('.customer_review').on('change', function (e) {
    var optionSelected = $("option:selected", this);
        var customer_review = this.value;
        var shop_name = $(this).children(":selected").attr("id");
         
         $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
            method: 'POST',
            url: '/admin/public/savereview',
            data: {
                'customer_review': customer_review,
                'shop_name' : shop_name,
            },
            success: function(response) {
                 $('.alert-success').show();
                $('.alert-success').html('<button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button><i class="ace-icon fa fa-check green"></i>  Customer Review Updated Successfully');

            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
           
});
    
    
    $('.update_text').on('click', function (e) {
        
        var form = $(this).parents('form:first').attr('name');   

        var customer_text = $('form[name="'+form+'"]>.customer_text').val();
        
        var shop_name = $('form[name="'+form+'"]>.shop_name').val();
        
         $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
            method: 'POST',
            url: '/admin/public/savecustomertext',
            data: {
                'customer_text': customer_text,
                'shop_name' : shop_name,
            },
            success: function(response) {
                 $('.alert-success').show();
                $('.alert-success').html('<button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button><i class="ace-icon fa fa-check green"></i>  Customer Text Updated Successfully');

            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
           
});
    
    
  

  
            
</script>

<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="{!! URL::to('/') !!}">Home</a>
							</li>
							<li class="active">Dashboard</li>
                            
						</ul><!-- /.breadcrumb -->

						
					</div>

					<div class="page-content">
						
						<div class="page-header">
							<h1>
								Shops
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									overview &amp; stats
								</small>
							</h1>
                            
                           

                            
						</div><!-- /.page-header -->
                        <div id="LoadingImage" style="margin-left:50% ; display:none;"><img src="assets/images/ajax-loader.gif"></div>
						<div class="row">
							<div class="col-xs-12">
                                <div class="alert alert-block alert-success" style="display:none;">
									

									
								</div>
                                
                                <a class="btn btn-primary" id="shops_csv" href="{!! URL::to('/shops_csv') !!}" target="_self">Download CSV</a>
                                
                                <div class="control-group pull-right">
                        
                                    <select class="form-control" id="select_type" name="select_type">
                                        <option value="shops" selected>Shops</option>
                                        <option value="deleted_shops">Deleted Shops</option>
                                    </select>
                        
                                </div>
                                
                                <br><br><br>
								<!-- PAGE CONTENT BEGINS -->
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Shop Name</th>
                                        <th>Currency</th>	
                                        <th>Email</th>
                                        <th>Shop Owner</th>
                                        <th>Timezone</th>
                                        <th>Seen</th>
                                        <th>Customer Review</th>
                                        <th>Customer Text</th>	
                                    </tr>
                                </thead>

                                <tbody>
                                    @php $number = 0 @endphp
                                   
                                    @if(count($shops) > 0)
                                    
                                    @foreach($shops as $shop)
                                    
                                    
                                    <tr id="{{ $shop->id }}">
                                       
                                        <td>{{ $shop->shop_name }}</td>
                                        <td>{{ $shop->currency }}</td>
                                        <td>{{ $shop->email }}</td>
                                        <td>{{ $shop->shop_owner }}</td>
                                        <td>{{ $shop->timezone }}</td>
                                        <td>{{ $shop->seen }}</td>
                                        <td><select id="customer_review" name="customer_review" class="customer_review" >
                                            <option value="yes" id="{{ $shop->shop_name }}" <?php if($shop->customer_review == 'yes'){ echo "selected='selected'"; } ?>>Yes</option>
                                            <option value="no"  id="{{ $shop->shop_name }}" <?php if($shop->customer_review == 'no'){ echo "selected='selected'"; } ?>>No</option>
                                            </select></td>
                                        <td>
                                            <form id="form{{ $number }}" name="form{{ $number }}">
                                            <input type="hidden" name="shop_name" id="shop_name" class="shop_name" value="{{ $shop->shop_name }}">
                                                   
                                            <textarea id="customer_text" name="customer_text" class="customer_text" rel="{{ $shop->shop_name }}">{{ $shop->customer_text }}</textarea>
                                        <button type="button" id="update" name="update" class="btn update_text">Update</button></form>
                                        </td>
                                          
                                    </tr>
                                   @php $number++ @endphp
                                    @endforeach			
                                     @endif	
                                </tbody>
                            </table>

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>