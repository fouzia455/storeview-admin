@extends('layouts.login-master')

@section('content')

	<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center">
								<h1>
									<i class="ace-icon fa fa-leaf green"></i>
									<span class="red"></span>
									<span class="white" id="id-text2">Control Panel</span>
								</h1>
								<h4 class="blue" id="id-company-text">&copy; StoreView</h4>
							</div>

							<div class="space-6"></div>

							<div class="position-relative">
								<div id="login-box" class="login-box visible widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header blue lighter bigger">
												<i class="ace-icon fa fa-coffee green"></i>
												Please Enter Your Information
											</h4>

											<div class="space-6"></div>

											<form  id="add_post" name="add_post" action="{!! URL::to('/dashboard') !!}" method="post">
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" class="form-control" placeholder="Enter Email" id="email" name="email" {{ $errors->has('title') ? 'class=has-error' : '' }} />
															<i class="ace-icon fa fa-user"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" class="form-control" placeholder="Enter Password" id="password" name="password" {{ $errors->has('password') ? 'class=has-error' : '' }} />
															<i class="ace-icon fa fa-lock"></i>
														</span>
													</label>

													<div class="space"></div>

													<div class="clearfix">
														
                                                         <input type="hidden" value="{{ Session::token() }}" name="_token">    
														<button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
															<i class="ace-icon fa fa-key"></i>
															<span class="bigger-110">Login</span>
														</button>
													</div>

													<div class="space-4"></div>
												</fieldset>
											</form>

											
										</div><!-- /.widget-main -->

										
									</div><!-- /.widget-body -->
								</div><!-- /.login-box -->

							</div><!-- /.position-relative -->

						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->

@endsection

@section('scripts')
<!--[if !IE]> -->
<script src="{!! URL::to('assets/js/jquery-2.1.4.min.js') !!}"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="{!! URL::to('assets/js/jquery-1.11.3.min.js') !!}"></script>
<![endif]-->
<script type="text/javascript">
    if('ontouchstart' in document.documentElement) document.write("<script src='{!! URL::to('assets/js/jquery.mobile.custom.min.js') !!}'>"+"<"+"/script>");
</script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
    jQuery(function($) {
        $('body').attr('class', 'login-layout light-login');
        $('#id-text2').attr('class', 'grey');
        $('#id-company-text').attr('class', 'blue');
    });

	var token = "{{ Session::token() }}";	
</script>

@endsection