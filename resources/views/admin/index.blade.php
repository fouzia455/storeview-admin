@extends('layouts.admin-master')

@section('content')

	<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar responsive ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>

				<div class="sidebar-shortcuts" id="sidebar-shortcuts">
					<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
						<button class="btn btn-success">
							<i class="ace-icon fa fa-signal"></i>
						</button>

						<button class="btn btn-info">
							<i class="ace-icon fa fa-pencil"></i>
						</button>

						<button class="btn btn-warning">
							<i class="ace-icon fa fa-users"></i>
						</button>

						<button class="btn btn-danger">
							<i class="ace-icon fa fa-cogs"></i>
						</button>
					</div>

					<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
						<span class="btn btn-success"></span>

						<span class="btn btn-info"></span>

						<span class="btn btn-warning"></span>

						<span class="btn btn-danger"></span>
					</div>
				</div><!-- /.sidebar-shortcuts -->

				<ul class="nav nav-list">
					<li class="active">
						<a href="{!! URL::to('/') !!}">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> Dashboard </span>
						</a>

						<b class="arrow"></b>
					</li>
                    
                    	<li class="">
						<a href="javascript:void(0)" onclick="ShopListings()">
							<i class="menu-icon fa fa-desktop"></i>
							<span class="menu-text">
								Shops
							</span>

						</a>

					</li>

					
				</ul><!-- /.nav-list -->

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>
           
            <div class="main-content listings">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="{!! URL::to('/') !!}">Home</a>
							</li>
							<li class="active">Dashboard</li>
                            
						</ul><!-- /.breadcrumb -->

						
					</div>

					<div class="page-content">
                        
                        
						
						<div class="page-header">
							<h1>
								Dashboard
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									overview &amp; stats
								</small>
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="alert alert-block alert-success">
									<button type="button" class="close" data-dismiss="alert">
										<i class="ace-icon fa fa-times"></i>
									</button>

									<i class="ace-icon fa fa-check green"></i>

									Welcome to
									<strong class="green">
										StoreView
										<small>Admin Panel</small>
									</strong>
								</div>
                                <div id="LoadingImage" style="margin-left:50% ; display:none;"><img src="assets/images/ajax-loader.gif"></div>
								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

        
            
			
			

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->


@endsection

@section('scripts')

		<!--[if !IE]> -->
		<script src="{!! URL::to('assets/js/jquery-2.1.4.min.js') !!}"></script>

		<!-- <![endif]-->

		<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='{!! URL::to('assets/js/jquery.mobile.custom.min.js') !!}'>"+"<"+"/script>");
		</script>
		<script src="{!! URL::to('assets/js/bootstrap.min.js') !!}"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="{!! URL::to('assets/js/excanvas.min.js') !!}"></script>
		<![endif]-->
		<script src="{!! URL::to('assets/js/jquery-ui.custom.min.js') !!}"></script>
		<script src="{!! URL::to('assets/js/jquery.ui.touch-punch.min.js') !!}"></script>
		<script src="{!! URL::to('assets/js/jquery.easypiechart.min.js') !!}"></script>
		<script src="{!! URL::to('assets/js/jquery.sparkline.index.min.js') !!}"></script>
		<script src="{!! URL::to('assets/js/jquery.flot.min.js') !!}"></script>
		<script src="{!! URL::to('assets/js/jquery.flot.pie.min.js') !!}"></script>
		<script src="{!! URL::to('assets/js/jquery.flot.resize.min.js') !!}"></script>

		<!-- ace scripts -->
		<script src="{!! URL::to('assets/js/ace-elements.min.js') !!}"></script>
		<script src="{!! URL::to('assets/js/ace.min.js') !!}"></script>



		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			var ShopListings = function(){
                $("#LoadingImage").show();
                  $.ajax({
                    headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					  },
                    method: 'GET',
                    url: '/admin/public/shops',
                    
                    success: function(response) {
                        $("#LoadingImage").hide();
                        var response 		= JSON.parse(response);
						
                        $(".listings").html('');
                       
                        $('.listings').append(response.shops);
                        
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $("#LoadingImage").hide();
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
              }  
            
       
            

		</script>

@endsection