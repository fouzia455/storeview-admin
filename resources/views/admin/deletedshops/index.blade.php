<!-- page specific plugin scripts -->
<script src="{!! URL::to('assets/js/jquery.dataTables.min.js') !!}"></script>
<script src="{!! URL::to('assets/js/jquery.dataTables.bootstrap.min.js') !!}"></script>

<script type="application/javascript">

jQuery(function($) {
				//initiate dataTables plugin
				var myTable = 
				$('#dynamic-table')
				//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
				.DataTable();
                } );
    
        
            $('#select_type').on('change', function (e) {
                var optionSelected = $("option:selected", this);
                var valueSelected = this.value;
                
                if(valueSelected == 'shops'){
                    $("#LoadingImage").show();
                  $.ajax({
                    headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					  },
                    method: 'GET',
                    url: '/admin/public/shops',
                    
                    success: function(response) {
                        $("#LoadingImage").hide();
                        var response 		= JSON.parse(response);
						
                        $(".listings").html('');
                       
                        $('.listings').append(response.shops);
                        
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $("#LoadingImage").hide();
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
                }
                else if(valueSelected == 'deleted_shops'){
                    $("#LoadingImage").show();
                  $.ajax({
                    headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					  },
                    method: 'GET',
                    url: '/admin/public/deletedshops',
                    
                    success: function(response) {
                        $("#LoadingImage").hide();
                        var response 		= JSON.parse(response);
						
                        $(".listings").html('');
                       
                        $('.listings').append(response.deletedshops);
                        
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $("#LoadingImage").hide();
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
                }
                
            });
    
   
          
</script>

<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="{!! URL::to('/') !!}">Home</a>
							</li>
							<li class="active">Dashboard</li>
                            
						</ul><!-- /.breadcrumb -->

						
					</div>

					<div class="page-content">
						
						<div class="page-header">
							<h1>
								Shops
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									overview &amp; stats
								</small>
							</h1>
                      
						</div><!-- /.page-header -->
                        <div id="LoadingImage" style="margin-left:50% ; display:none;"><img src="assets/images/ajax-loader.gif"></div>
						<div class="row">
							<div class="col-xs-12">
                                
                                
                                <a class="btn btn-primary" id="shops_csv" href="{!! URL::to('/shops_csv') !!}" target="_self">Download CSV</a>
                                
                                <div class="control-group pull-right">
                        
                                    <select class="form-control" id="select_type" name="select_type">
                                        <option value="shops" >Shops</option>
                                        <option value="deleted_shops" selected>Deleted Shops</option>
                                    </select>
                        
                                </div>
                                
                                <br><br><br>
								<!-- PAGE CONTENT BEGINS -->
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Shop Name</th>	
                                        <th>Email</th>
                                        <th>Shop Owner</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @if(count($deletedshops) > 0)
                                    @foreach($deletedshops as $shop)
                                    <tr>
                                        <td>{{ $shop->shop_name }}</td>
                                        <td>{{ $shop->email }}</td>
                                        <td>{{ $shop->shop_owner }}</td>
                                        
                                    </tr>
                                    @endforeach			
                                     @endif	
                                </tbody>
                            </table>

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>