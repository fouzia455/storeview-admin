<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Login Page - Ace Admin</title>

		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="{!! URL::to('assets/css/bootstrap.min.css') !!}" />
		<link rel="stylesheet" href="{!! URL::to('assets/font-awesome/4.5.0/css/font-awesome.min.css') !!}" />

		<!-- text fonts -->
		<link rel="stylesheet" href="{!! URL::to('assets/css/fonts.googleapis.com.css') !!}" />

		<!-- ace styles -->
		<link rel="stylesheet" href="{!! URL::to('assets/css/ace.min.css') !!}" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="{!! URL::to('assets/css/ace-part2.min.css') !!}" />
		<![endif]-->
		<link rel="stylesheet" href="{!! URL::to('assets/css/ace-rtl.min.css') !!}" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="{!! URL::to('assets/css/ace-ie.min.css') !!}" />
		<![endif]-->

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="{!! URL::to('assets/js/html5shiv.min.js') !!}"></script>
		<script src="{!! URL::to('assets/js/respond.min.js') !!}"></script>
		<![endif]-->
        <title>Admin Panel - Login</title>	 
    </head>
    <body class="login-layout">
			
			@yield('content')
			
			<script type="application/javascript">
		
				var baseUrl  = "{{ URL::to('/') }}";
			</script>
			@yield('scripts')
			
    </body>
</html>
