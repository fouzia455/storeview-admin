<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*====== Frontend Route call ====*/
Route::get('/', 'AdminController@getIndex');

Route::post('/dashboard', 'AdminController@postLogin');

Route::get('/logout', 'AdminController@getLogout');

Route::get('/shops', 'ShopController@getShopListings');

Route::get('/deletedshops', 'ShopController@getDeletedShopListings');

Route::get('/shops_csv', 'ShopController@shopCSV');

Route::get('/deleted_shops_csv', 'ShopController@deletedshopCSV');

Route::post('/savereview', 'ShopController@saveCustomerReview');

Route::post('/savecustomertext', 'ShopController@saveCustomerText');




