<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = new \App\Posts();
		$posts->title	= ('Test Post');
		$posts->description	= ('Hello World');
		$posts->save();
    }
}
